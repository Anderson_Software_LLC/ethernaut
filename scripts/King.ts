import { ethers } from "hardhat";
import { submitInstance, fetchInstanceAddress } from "./helpers/Ethernaut";

const LEVEL_ADDRESS = "0x25141B6345378e7558634Cf7c2d9B8670baFA417";

async function attack() {
  // Contract Setup
  const [sender] = await ethers.getSigners();
  const victimAddress = await fetchInstanceAddress(LEVEL_ADDRESS);
  const victim = await ethers.getContractAt("King", victimAddress);

  // Attack

  // Challenge Submission
  await submitInstance(victimAddress);
}

attack()
  .then(() => console.log("Attack has been completed"))
  .catch((error) => console.error("Attack encountered an error: ", error));
