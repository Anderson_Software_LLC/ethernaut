import { ethers } from "hardhat";
import { BigNumber } from "ethers";
import {
  Ethernaut,
  LevelInstanceCreatedLogEvent,
} from "../../typechain-types/contracts/Helpers/Ethernaut";

// Address of Ethernaut Control Contract on Goerli Test Network
const ETHERNAUT_ADDRESS = "0x42E7014a9D1f6765e76fA2e69532d808F2fe27E3";

// Typings for Internal EmittedInstanceData structure used in private variable
// of Ethernaut Control Contract.
interface EmittedInstanceData {
  player: string;
  level: string;
  completed: boolean;
}

// Error to raise if an instance is not found
export const NoInstanceFoundError = Error("No Instance Found");

/**
 * Function to read the private mapping data emittedInstances because OpenZeppelin
 * did not care to make finding your current instances very easy did they?
 * @param instanceAddress The instance you are looking up data for to map to a specific level
 * @returns Returns an EmittedInstanceData object
 */
async function FetchEmittedInstances(
  instanceAddress: string
): Promise<EmittedInstanceData> {
  // The keyslot for emittedInstances is 2 with 0 held by the owner address and 1 held by registeredLevels
  const abiCoder = new ethers.utils.AbiCoder();
  // Mapping Key Slot is computed as keccak256(instanceAddress, keySlot) where both are fully padded values
  const storageSlot = ethers.BigNumber.from(
    ethers.utils.keccak256(
      ethers.utils.concat([
        abiCoder.encode(["address"], [instanceAddress]), // They key of the map
        abiCoder.encode(["uint256"], [2]), // The keyslot of the mapping
      ])
    )
  );
  // Fetch storage for internal data structure at the calculated key slot
  const storage1Promise = ethers.provider.getStorageAt(
    ETHERNAUT_ADDRESS,
    storageSlot
  );
  const storage2Promise = ethers.provider.getStorageAt(
    ETHERNAUT_ADDRESS,
    storageSlot.add(1)
  );
  const [storage1, storage2] = await Promise.all([
    storage1Promise,
    storage2Promise,
  ]);
  const playerAddressBinary = storage1;
  const levelAddressBinary = ethers.utils.hexZeroPad(
    ethers.utils.hexDataSlice(storage2, 12, 32),
    32
  );

  const levelCompletionBinary = ethers.utils.hexZeroPad(
    ethers.utils.hexDataSlice(storage2, 11, 12),
    32
  );
  // Concat the three byte32 values into one single data blob
  const storageData = ethers.utils.concat([
    playerAddressBinary,
    levelAddressBinary,
    levelCompletionBinary,
  ]);
  // Decode the dataBlob into the types address (string) and bool (boolean)
  const result = abiCoder.decode(["address", "address", "bool"], storageData);
  // Return typed as the interface EmmittedInstanceData
  return {
    player: result[0],
    level: result[1],
    completed: result[2],
  };
}

/**
 * This function will find your last created instance of a level OR create a new instance if one
 * is not found. WARNING: The code to find your last instance is rather difficult and it appears
 * OpenZeppelin just stores the last instance in localstorage on your browser. If you use this to
 * create a level it will not show as completed on the Ethernaut website so its almost always
 * better to create the instance in the browser.
 * @param levelAddress Address of the level to search for instances or create
 * @returns The address of the instance found or created
 */
export async function fetchOrCreateInstance(
  levelAddress: string,
  value?: BigNumber
): Promise<string> {
  try {
    // Raises NoInstanceFoundError when there are no instances that are in progress
    return await fetchInstanceAddress(levelAddress);
  } catch (Error) {
    if (Error === NoInstanceFoundError) {
      // If no instances exist lets create a new one
      console.log(
        "No existing instance of level was found, creating new instance"
      );
      return await createInstance(levelAddress, value);
    }
    // If error was of other type lets raise it again
    throw Error;
  }
}

/**
 * This function will create a new instance of a level given by address.
 * WARNING: The code to find your last instance is rather difficult and it appears
 * OpenZeppelin just stores the last instance in localstorage on your browser. If you use this to
 * create a level it will not show as completed on the Ethernaut website so its almost always
 * better to create the instance in the browser.
 * @param levelAddress Address of the level to create
 * @returns The address of the instance just created
 */
export async function createInstance(
  levelAddress: string,
  value?: BigNumber
): Promise<string> {
  // Create Instance
  console.log(`Creating instance for level at address: ${levelAddress}`);
  const ethernaut = await ethers.getContractAt("Ethernaut", ETHERNAUT_ADDRESS);
  const levelCreationTx = await ethernaut.createLevelInstance(levelAddress, {
    value: value,
    ...TX_VALUES,
  });
  const result = await levelCreationTx.wait();
  // If the level was created it will emit an event which has the new instance address
  // However many events could be in this TX so we must find the specific Level InstanceCreated event
  for (const levelCreatedEvent of result.events as unknown as LevelInstanceCreatedLogEvent[]) {
    if (levelCreatedEvent.event === "LevelInstanceCreatedLog") {
      const instanceAddress = levelCreatedEvent.args.instance;
      console.log(`Instance created at address ${instanceAddress}`);
      return instanceAddress;
    }
  }
  throw Error("Could not create instance");
}

/**
 * This function will look through all createdInstance events for a user and then map those to levels
 * when it finds an instance that matches the levelAddress param with the completed flag of false it
 * will return that instance address. If it can't find a match it will throw an exception. Because
 * of how OpenZeppelin coded this contract, programmatically finding the latest instance of a level
 * is very inefficient and requires many calls to the blockchain node.
 * @param levelAddress The level address we are trying to find an instance of
 * @returns An address pointing to the instance of this level
 */
export async function fetchInstanceAddress(
  levelAddress: string
): Promise<string> {
  const [sender] = await ethers.getSigners();
  // Fetch Instance
  const ethernaut = await ethers.getContractAt("Ethernaut", ETHERNAUT_ADDRESS);
  console.log(`Fetching instance address for level: ${levelAddress}`);
  // Get created instances for player
  const createdInstancesFilter = ethernaut.filters.LevelInstanceCreatedLog(
    sender.address
  );
  // Fetches all created instances events
  const createdInstances = await ethernaut.queryFilter(createdInstancesFilter);
  // We loop in reverse order (newest events first)
  for (let i = createdInstances.length - 1; i >= 0; i--) {
    const instance = createdInstances[i];
    const instanceAddress = instance.args.instance;
    // Because the event does not have the levelAddress as part of it we have to read
    // private storage of the Ethernaut contract to match the instance address to
    // a level
    const EmittedInstanceData = await FetchEmittedInstances(instanceAddress);
    // If the level matches the levelAddress param AND the level has not been completed
    // we return this instance address
    if (
      EmittedInstanceData.level === levelAddress &&
      EmittedInstanceData.completed === false
    ) {
      console.log(`Instance found at address: ${instanceAddress}`);
      return instanceAddress;
    }
  }
  // If we have not returned any instance addresses then there is no current incomplete
  // instance of this level and we should throw an exception
  throw NoInstanceFoundError;
}

/**
 * This structure contains the EIP-1559 transaction data for Hardhat-Ethers to send a
 * type 2 (EIP-1559) transaction with enough gas and priority to ensure it quickly processes
 * You should include this struct in every transaction/deployment for example
 * ```ts
 *  await victim.someMethod(SomeInput, TX_VALUES)
 * ```
 *
 * for a standard method call or use the spread operator if you need extra values
 *
 * ```ts
 *  await victim.someMethod(SomeInput, {gasLimit: 500_000, ...TX_VALUES})
 * ```
 */
export const TX_VALUES = {
  maxFeePerGas: ethers.utils.parseUnits("300", "gwei"),
  maxPriorityFeePerGas: ethers.utils.parseUnits("5", "gwei"),
};

/**
 * This function allows you to submit an instance of a level to ethernaut once its complete.
 * If the submission is accepted it will return true and if not it will return false.
 * @param instance The instance address of the level you wish to submit to Ethernaut
 * @returns true if accepted false if rejected (level not complete)
 */
export async function submitInstance(instance: string): Promise<boolean> {
  // Submit Instance
  const ethernaut = await ethers.getContractAt("Ethernaut", ETHERNAUT_ADDRESS);
  console.log(`Submitting level for instance: ${instance}`);
  const resultSubmission = await ethernaut.submitLevelInstance(instance, {
    ...TX_VALUES,
    gasLimit: 500_000,
  });
  const result = await resultSubmission.wait();
  // If the transaction has a LevelCompletedLog then the submission was successful
  const events = result.events;
  if (events?.length == 1) {
    const event = events[0];
    if (event.event == "LevelCompletedLog") {
      console.log(
        `Ethernaut submission confirmed on Block: ${result.blockNumber} with Tx Hash: ${result.transactionHash}`
      );
      return true;
    }
  }
  console.log(
    `!!!Ethernaut submission [FAILED], submitted on Block: ${result.blockNumber} with Tx Hash: ${result.transactionHash}!!!`
  );
  return false;
}
