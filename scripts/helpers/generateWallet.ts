import { ethers } from 'ethers';

async function generateWallet() {
    const wallet = ethers.Wallet.createRandom();
    console.log("Generated New Wallet", {
        "Address": wallet.address,
        "Private Key": wallet._signingKey(),
        "Mnemonic": wallet._mnemonic()
    })
}

generateWallet().then(() =>{}).catch((err) => console.error("An unexpected error occurred: ", err));