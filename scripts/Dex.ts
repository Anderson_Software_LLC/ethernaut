import { ethers } from "hardhat";
import { submitInstance, fetchInstanceAddress } from "./helpers/Ethernaut";

const LEVEL_ADDRESS = "0x8E500A9082D26dfA7CCdecf0391E0b93B9470266";

async function attack() {
  // Contract Setup
  const [sender] = await ethers.getSigners();
  const victimAddress = await fetchInstanceAddress(LEVEL_ADDRESS);
  const victim = await ethers.getContractAt("Dex", victimAddress);

  // Attack

  // Challenge Submission
  await submitInstance(victimAddress);
}

attack()
  .then(() => console.log("Attack has been completed"))
  .catch((error) => console.error("Attack encountered an error: ", error));
