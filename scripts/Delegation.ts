import { ethers } from "hardhat";
import { submitInstance, fetchInstanceAddress } from "./helpers/Ethernaut";

const LEVEL_ADDRESS = "0x31C4D3a9e0ED12A409cF3C84ad145331aB487D3F";

async function attack() {
  // Contract Setup
  const [sender] = await ethers.getSigners();
  const victimAddress = await fetchInstanceAddress(LEVEL_ADDRESS);
  const victim = await ethers.getContractAt("Delegation", victimAddress);

  // Attack

  // Challenge Submission
  await submitInstance(victimAddress);
}

attack()
  .then(() => console.log("Attack has been completed"))
  .catch((error) => console.error("Attack encountered an error: ", error));
