import { ethers } from "hardhat";
import { submitInstance, fetchInstanceAddress } from "./helpers/Ethernaut";

const LEVEL_ADDRESS = "0x40b8Df5575a55b2f0c55E6e18838AEC7B82aebD7";

async function attack() {
  // Contract Setup
  const [sender] = await ethers.getSigners();
  const victimAddress = await fetchInstanceAddress(LEVEL_ADDRESS);
  const victim = await ethers.getContractAt("Reentrance", victimAddress);

  // Attack

  // Challenge Submission
  await submitInstance(victimAddress);
}

attack()
  .then(() => console.log("Attack has been completed"))
  .catch((error) => console.error("Attack encountered an error: ", error));
