import { ethers } from "hardhat";
import { submitInstance, fetchInstanceAddress } from "./helpers/Ethernaut";

const LEVEL_ADDRESS = "0x3c34A342b2aF5e885FcaA3800dB5B205fEfa3ffB";

/**
 * Having Trouble?
 * You may find hardhat-trace to be useful; you can have it display traces of smart contract transactions!
 *    await hh.run("trace", {
 *      hash: transactionHash,
 *      fulltrace: true,
 *      opcodes: "SHA3", // Hint Hint
 *    });
 */

async function attack() {
  // Contract Setup
  const [sender] = await ethers.getSigners();
  const victimAddress = await fetchInstanceAddress(LEVEL_ADDRESS);
  const victim = await ethers.getContractAt("AlienCodex", victimAddress);

  // Attack

  // Challenge Submission
  await submitInstance(victimAddress);
}

attack()
  .then(() => console.log("Attack has been completed"))
  .catch((error) => console.error("Attack encountered an error: ", error));
