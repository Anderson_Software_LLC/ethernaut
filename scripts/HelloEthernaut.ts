import { ethers } from "hardhat";
import { submitInstance, fetchInstanceAddress } from "./helpers/Ethernaut";

const LEVEL_ADDRESS = "0xBd886a37faD1f596221f33ca568122815ED48c81";

async function attack() {
  // Contract Setup
  const [sender] = await ethers.getSigners();
  const victimAddress = await fetchInstanceAddress(LEVEL_ADDRESS);
  const victim = await ethers.getContractAt("Instance", victimAddress);

  // Attack

  // Challenge Submission
  await submitInstance(victimAddress);
}

attack()
  .then(() => console.log("Attack has been completed"))
  .catch((error) => console.error("Attack encountered an error: ", error));
