# Ethernaut Challenges

This project is a cloneable base for performing the Ethernaut Challenges by the OpenZeppelin team https://ethernaut.openzeppelin.com/.

## Getting Started

To get started clone this repo onto your local computer, you will need NodeJS and NPM already installed. Setup the project dependencies with npm install

```shell
npm install
```

It is strongly advised you generate a new private key for these challenges and do not use an old one. To generate a new public/private key simply run

```shell
npx hardhat run scripts/helpers/generateWallet.ts
```

It will generate an output like so:

```
Generated New Wallet {
  Address: '0x4d62404B611f556Be3f545bFa53d2a924e6bD7a3',
  'Private Key': SigningKey {
    curve: 'secp256k1',
    privateKey: '0x1919c67c5944fc1aff0b9c8c042a9f8e88476cc9f9fe6795021d37e9dc9a7a83',
    publicKey: '0x0480b2e5133f26b898d6fdd3509d31b4d6fd673718466da21193f9511a8e0c56b24e22b2f1c110db4e10f1300734f5f9f6e04dade24296d604904c32e2f25ce8be',
    compressedPublicKey: '0x0280b2e5133f26b898d6fdd3509d31b4d6fd673718466da21193f9511a8e0c56b2',
    _isSigningKey: true
  },
  Mnemonic: {
    phrase: 'renew knock solution grass latin color kid adapt oval alpha nose equal',
    path: "m/44'/60'/0'/0/0",
    locale: 'en'
  }
}
```

In your browser make sure you have a Web3 wallet such as Metamask or the built in Brave Wallet on Brave Browser. You can import the newly generated private key
to your wallet so that hardhat and your browser are sharing the same credentials.

Rename the `example.env` file `.env` and place your newly generated Mnemonic phrase into the line labeled MNEMONIC=...

Set GOERLI_TESTNET=... to a Goerli Archive Node that you have access to, if you don't currently have access to a archive node Alchemy offers free (rate limited) archive nodes
that you can use. Referral link will give you a $100 credit when you create your account https://alchemy.com/?r=233b41a303f5aefa.

In the browser chose a challenge you want to do, click on the `Get New Instance` button. Once the transaction is confirmed on chain you are ready to go.
This repo has a contracts folder with all the level contracts in it, you can create new contracts that are used as part of your attacks in the Attacks folder.
The scripts to interact with or deploy your own contracts can be found in scripts/. Each challenge has an already created scripts file which will fetch the instance
address automatically as well as submit the level for review at the end of the script. Each script file will give you an ethersjs contract object of the deployed ethernaut instance
to interact with.

When you are ready to run your script for a level simply call:

```shell
npx hardhat run scripts/<script file>
```

Hardhat will generate the typings with typechain, compile the contracts, complie the typescript, spin up a local EVM chain that is forked from Goerli and simulate the script locally.
If the scripts pass you can run them live on the testnet by passing in the network option:

```shell
npx hardhat run scripts/<script file> --network goerli
```

This way you can test rapidly and without waisting test eth locally and only submit your level when you know it will work.

Best of luck!
