import { task } from "hardhat/config";

// Address of Ethernaut Control Contract on Goerli Test Network
export const ETHERNAUT_ADDRESS = "0x42E7014a9D1f6765e76fA2e69532d808F2fe27E3";

interface ProgressArgs {
  userAddress: string;
}

interface Level {
  level: number;
  name: string;
}

interface Levels {
  [key: string]: Level;
}

const Levels: Levels = {
  "0xBd886a37faD1f596221f33ca568122815ED48c81": {
    level: 0,
    name: "Hello Ethernaut",
  },
  "0x6F9cf195B9B4c1259E8FCe5b4e30F7142f779DeA": { level: 1, name: "Fallback" },
  "0x40F5513a90fb7e2ac2C3E12A6d16B9279D1e94Ed": { level: 2, name: "Fallout" },
  "0xae9677ff69efB3C1B9559C8F2A9ED6a2212148e3": { level: 3, name: "Coin Flip" },
  "0x466BDd41a04473A01031C9D80f61A9487C7ef488": { level: 4, name: "Telephone" },
  "0xDc0c34CFE029b190Fc4A6eD5219BF809F04E57A3": { level: 5, name: "Token" },
  "0x31C4D3a9e0ED12A409cF3C84ad145331aB487D3F": {
    level: 6,
    name: "Delegation",
  },
  "0x20B5c742dD8A63400644Ba85dd48E8FDB6908A7A": { level: 7, name: "Force" },
  "0x78BA1a1DD8833A4a20ecAc0Db8f3aCD8A9211beD": { level: 8, name: "Vault" },
  "0x25141B6345378e7558634Cf7c2d9B8670baFA417": { level: 9, name: "King" },
  "0x40b8Df5575a55b2f0c55E6e18838AEC7B82aebD7": {
    level: 10,
    name: "Re-entrancy",
  },
  "0xe1B98E88F060a661029158b0c45c2B8424C9c83f": { level: 11, name: "Elevator" },
  "0x2aa5685ffd9e8e4897caf92855C1959d82DA5E36": { level: 12, name: "Privacy" },
  "0xD3ebB53C3c2126D14383a6c8519C87106772607e": {
    level: 13,
    name: "Gatekeeper One",
  },
  "0xe8F8591475E425BA05336fa27d9810cd0c3eEe69": {
    level: 14,
    name: "Gatekeeper Two",
  },
  "0x545d848827bD9e0E30794a9E53f5ab04EA71d78a": {
    level: 15,
    name: "Naught Coin",
  },
  "0x57d122d0355973dA78acF5138aE664548bB2CA2b": {
    level: 16,
    name: "Preservation",
  },
  "0x026330B3Eb548B28F4BACbe101431E74597b0aa6": { level: 17, name: "Recovery" },
  "0xaCB258afa213Db8E0007459f5d3851c112d2fA8d": {
    level: 18,
    name: "Magic Number",
  },
  "0x3c34A342b2aF5e885FcaA3800dB5B205fEfa3ffB": {
    level: 19,
    name: "Alien Codex",
  },
  "0x478f3476358Eb166Cb7adE4666d04fbdDB56C407": { level: 20, name: "Denial" },
  "0xA62fE5344FE62AdC1F356447B669E9E6D10abaaF": { level: 21, name: "Shop" },
  "0x8E500A9082D26dfA7CCdecf0391E0b93B9470266": { level: 22, name: "Dex" },
  "0xc06a07059564DF5034AfA6d3C8BB144663cbFE2e": { level: 23, name: "Dex Two" },
  "0xba6F0B5784B6580790584A553f6e4a3483a915c3": {
    level: 24,
    name: "Puzzle Wallet",
  },
  "0x492e18ddBd7591638453d2f1B1847F86711105C8": {
    level: 25,
    name: "Motorbike",
  },
  "0x990f8E9fb0a9f990F064D863A35FDE6E68808Aff": {
    level: 26,
    name: "Double Entry Point",
  },
  "0x4A5351c513127F155aF034a9fA17CD848129F233": {
    level: 27,
    name: "Good Samaritan",
  },
};

task("progress", "Show users progress in the current Ethernaut challenges")
  .addOptionalParam(
    "userAddress",
    "Set a user address to view progress of, otherwise default to your account"
  )
  .setAction(async (args: ProgressArgs, hre) => {
    const ethers = hre.ethers;
    // Get user public address
    const address = await (async () => {
      if (args.userAddress) {
        return args.userAddress;
      }
      const [user] = await ethers.getSigners();
      return user.address;
    })();

    const ethernaut = await ethers.getContractAt(
      "Ethernaut",
      ETHERNAUT_ADDRESS
    );

    const completedFilter = ethernaut.filters.LevelCompletedLog(address);
    const completedEvents = await ethernaut.queryFilter(completedFilter);

    // Create an array to store completed levels and a map to store incomplete levels so its easy to index and remove entries
    const completedLevels: Level[] = [];
    const incompleteLevelsMap = new Map(Object.entries(Levels));

    // Loop through all events transferring completed Levels to the completed array and removing completed levels from the incomplete array
    for (const event of completedEvents) {
      const level = incompleteLevelsMap.get(event.args.level);
      if (level === undefined) {
        // The level was done a second time so we should skip it
        continue;
      }
      completedLevels.push(level);
      incompleteLevelsMap.delete(event.args.level);
    }
    const incompleteLevels: Level[] = [];

    // Loop through remaining levels and put it in an array
    for (const [_, entry] of incompleteLevelsMap) {
      incompleteLevels.push(entry);
    }

    // Sort the arrays
    function levelSorter(a: Level, b: Level): number {
      return a.level - b.level;
    }

    completedLevels.sort(levelSorter);
    incompleteLevels.sort(levelSorter);

    console.log("\nLevels Completed: \n");
    for (const level of completedLevels) {
      console.log(`Level Number: ${level.level}, Level Name: ${level.name}`);
    }
    console.log("\nLevels Not Completed: \n");
    for (const level of incompleteLevels) {
      console.log(`Level Number: ${level.level}, Level Name: ${level.name}`);
    }

    const totalLevels = completedLevels.length + incompleteLevels.length;

    console.log(
      `\n\nUser has completed ${completedLevels.length} of ${totalLevels}. ${(
        (completedLevels.length / totalLevels) *
        100
      ).toFixed(2)}% Completed\n`
    );
  });
