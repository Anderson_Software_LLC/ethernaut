import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import * as dotenv from "dotenv";
import "./tasks/progress";

import "hardhat-tracer";
dotenv.config();

const GOERLI_TESTNET = process.env.GOERLI_TESTNET ?? false;
const mnemonic = process.env.MNEMONIC ?? false;

if (GOERLI_TESTNET === false) {
  console.error(`
  Ethernaut challenges in hardhat require access to a goerli archive node. Either set one up yourself and 
  set the HTTP/S endpoint in your environment variable as GOERLI_TESTNET=https://eth-goerli.g.alchemy.com/v2/<key_here>
  or get access to an online service such as alchemy (https://alchemy.com/?r=233b41a303f5aefa referral
  link gives you a $100 credit) which offers free archive node endpoints (which will be more then enough for Ethernaut)
   when under their free tier.
  `);
  throw Error("No Goerli Endpoint");
}

if (mnemonic === false) {
  console.error(
    `
    Ethernaut challenges require a goerli private key to submit transaction on the blockchain with. You can use a DEVELOPMENT
    key you generated on your own and set the MNEMONIC value equal to the backup words of the key in your .env file or you can 
    generate a new one (recommended) for these challenges using the generateWallet script (npx hardhat run scripts/helpers/generateWallets.ts).
    `
  );
  throw Error("No Private Key");
}

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: "0.5.3",
        settings: {
          optimizer: {
            enabled: true,
            runs: 1000,
          },
        },
      },
      {
        version: "0.6.12",
        settings: {
          optimizer: {
            enabled: true,
            runs: 1000,
          },
        },
      },
      {
        version: "0.8.12",
        settings: {
          optimizer: {
            enabled: true,
            runs: 1000,
          },
        },
      },
    ],
  },
  networks: {
    hardhat: {
      forking: {
        url: GOERLI_TESTNET,
        enabled: true,
      },
      accounts: {
        mnemonic: mnemonic,
      },
    },
    goerli: {
      url: GOERLI_TESTNET,
      accounts: {
        mnemonic: mnemonic,
      },
    },
  },
};

export default config;
